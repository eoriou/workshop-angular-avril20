import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Dashboard } from '../models/dashboard.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DashboardResolverService implements Resolve<Dashboard> {

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Dashboard> | Promise<Dashboard> | Dashboard {
        const id = route.paramMap.get('id');
        const dashboard = {id: id, totalMargin: 12};
        return dashboard;
    }
}
