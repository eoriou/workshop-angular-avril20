import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Dashboard } from '../../models/dashboard.model';

@Component({
    selector: 'home-dashboard-page',
    templateUrl: './home-dashboard-page.component.html',
    styleUrls: ['./home-dashboard-page.component.scss']
})
export class HomeDashboardPageComponent implements OnInit {

    public dashboard: Dashboard;

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.route.data
            .pipe(
                map(data => data['dashboard'])
            )
            .subscribe(dashboard => this.dashboard = dashboard);
    }
}
