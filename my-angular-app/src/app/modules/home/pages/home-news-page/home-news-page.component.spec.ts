import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeNewsPageComponent } from './home-news-page.component';

describe('HomeNewsPageComponent', () => {
  let component: HomeNewsPageComponent;
  let fixture: ComponentFixture<HomeNewsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeNewsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeNewsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
