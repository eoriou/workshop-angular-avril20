import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { HomeDashboardPageComponent } from './pages/home-dashboard-page/home-dashboard-page.component';
import { HomeNewsPageComponent } from './pages/home-news-page/home-news-page.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HomeRoutingModule } from './home-routing.module';

@NgModule({
    declarations: [
        CapitalizePipe,
        HomePageComponent,
        HomeDashboardPageComponent,
        HomeNewsPageComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        HomeRoutingModule
    ]
})
export class HomeModule {
}
