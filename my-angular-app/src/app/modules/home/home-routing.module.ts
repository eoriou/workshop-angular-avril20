import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { HomeDashboardPageComponent } from './pages/home-dashboard-page/home-dashboard-page.component';
import { DashboardResolverService } from './services/dashboard-resolver.service';
import { HomeNewsPageComponent } from './pages/home-news-page/home-news-page.component';

const routes: Routes = [
    {
        path: '',
        component: HomePageComponent,
        children: [
            {
                path: 'dashboard/:id',
                component: HomeDashboardPageComponent,
                resolve: {
                    dashboard: DashboardResolverService
                }
            },
            {path: 'news', component: HomeNewsPageComponent}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule {
}
