import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

    public transform(value: string, arg1: number, arg2: number): string {
        return value[0].toUpperCase() + value.toLowerCase().substr(1);
    }
}
