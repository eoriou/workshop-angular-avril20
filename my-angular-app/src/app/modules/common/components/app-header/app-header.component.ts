import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { User } from '../../models/user.model';

@Component({
    selector: 'app-header',
    templateUrl: './app-header.component.html',
    styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {

    @Input() public user: User;
    @Output() public logout: EventEmitter<string> = new EventEmitter<string>();

    public subject: Subject<string> = new Subject<string>();

    ngOnInit(): void {
    }

    public onLogout(): void {
        this.logout.emit('logout');
    }
}
