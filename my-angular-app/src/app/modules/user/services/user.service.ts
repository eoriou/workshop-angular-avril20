import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../common/models/user.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private httpClient: HttpClient) {
    }

    public fetchUsers(): Observable<User[]> {
        return this.httpClient.get<User[]>('/api/users');
    }
}
