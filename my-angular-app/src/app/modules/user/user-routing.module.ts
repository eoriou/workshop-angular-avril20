import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { UsersEditPageComponent } from './pages/users-edit-page/users-edit-page.component';

const routes: Routes = [
    {
        path: '',
        component: UsersPageComponent
    },
    {
        path: ':login/edit',
        component: UsersEditPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule {
}
