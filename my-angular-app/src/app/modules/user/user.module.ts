import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { UserRoutingModule } from './user-routing.module';
import { UsersEditPageComponent } from './pages/users-edit-page/users-edit-page.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        UsersPageComponent,
        UsersEditPageComponent
    ],
    imports: [
        CommonModule,
        UserRoutingModule,
        FormsModule
    ]
})
export class UserModule {
}
