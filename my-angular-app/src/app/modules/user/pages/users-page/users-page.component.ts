import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../../common/models/user.model';

@Component({
    selector: 'users-page',
    templateUrl: './users-page.component.html',
    styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {

    public users: User[];

    constructor(private userService: UserService) {
    }

    ngOnInit(): void {
        this.userService.fetchUsers()
            .subscribe(users => this.users = users);
    }
}
