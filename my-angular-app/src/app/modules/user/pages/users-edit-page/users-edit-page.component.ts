import { Component, OnInit } from '@angular/core';
import { User } from '../../../common/models/user.model';

@Component({
    selector: 'users-edit-page',
    templateUrl: './users-edit-page.component.html',
    styleUrls: ['./users-edit-page.component.scss']
})
export class UsersEditPageComponent implements OnInit {

    public user: User;

    ngOnInit(): void {
        this.user = {login: 'eoriou'};
    }

    public update(user: User) {
        console.log('update');
    }
}
